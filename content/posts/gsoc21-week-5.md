---
title: "digiKam: GSoC 2021 Week 4-5"
date: 2021-07-11T22:16:46+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Week 5 of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/c/c8/Mascot_konqi-app-system.png/400px-Mascot_konqi-app-system.png">}}

Hi all! I wasn't able to write last week. So this post has news from last 2 weeks. 

digiKam is now fully ported to __Qt 5.15__ (which is a prerequisite for Qt6). Merge requests that were under review have been merged, namely:

- [Remove Qt X11 Extras module](https://invent.kde.org/graphics/digikam/-/merge_requests/107)
- [Remove deprecated QMutex usages](https://invent.kde.org/graphics/digikam/-/merge_requests/108)
- [Replace removed QDateTime constructor with new function](https://invent.kde.org/graphics/digikam/-/merge_requests/110)
- [Port dbjobsthread.cpp to not use deprecated const_iterator "+" operator](https://invent.kde.org/graphics/digikam/-/merge_requests/111)
- [Isolate rajce plugin](https://invent.kde.org/graphics/digikam/-/merge_requests/112)

These were all part of porting to Qt5.15.

### The real challenge

Last two weeks was the first time I started planning to build digiKam with Qt6. A major problem was that digiKam used 
__Qt Webengine/Qt Webkit__ to have support of in-built browser for displaying online documents and webservice authorization
steps.

__Qt Webengine/Qt Webkit__ modules, as of now, are not available in __Qt 6.1.2__ and will only be stable with release __Qt 6.2__, expected to arrive in September 2021. It is not trivial to isolate code that dependeds on this module. 
I thought about some solutions like building with _alpha_ __Qt 6.2__ release. It failed. Me and my mentors had a long discussion about the solution and Gilles suggested that writing a _dummy API_ for now is a good solution.

### What is a _dummy API_? 

This is [WebBrowserDlg](https://invent.kde.org/graphics/digikam/-/blob/master/core/libs/dialogs/webbrowserdlg.h) class based on `QDialog` currently 
used in digiKam to display webcontent using `QWebEngine`. To be able to build code with _Qt6_ we have to have this 
class and functions to be used by clients. For that, we create a class `DNoWebDialog` and fill it with functions, 
signals that `WebBrowserDlg` offers to clients. Then we create empty definitions of those functions and replace the 
client code that used `WebBrowserDlg` with `DNoWebDialog` when no web module is found by CMake. 

Also, the `CMakeLists.txt` files have to be configured to not try to build code with `QWebEngine` classes when it is not
present. Rather build it with out _Dummy API_. This solves our temporary compilation problem. Also, when __Qt 6.2__ will
be released, then the code will automatically use the _real_ `QWebEngine` API instead of our own, thus enabling features
and support for online documents in digiKam as before.

This was completed in this [merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/114).

### What is next?

digiKam is huge with a lot of CMake files. I am currently porting the cmake files to be able to use Qt6. I have made 
progress in the _dummy API_ merge request but there are a lot of other targets that need to ported to use Qt6. It will
be completed in the next few days. `libO2`, the library for OAuth authentication used by webservices has it's separate cmake files. I am currently working on it and it should build easily with Qt6 without hassle.

I would also probably have to build KF5 with Qt6 as well. Some people have already done it in the community. In the 
coming days, we should be able to build digiKam with Qt6.

Thanks for reading!


