---
title: "digiKam: GSoC 2021 Week 7"
date: 2021-08-01T22:12:10+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Week 7 of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/e/e7/Mascot_konqi-app-office.png/400px-Mascot_konqi-app-office.png">}}

### Issue with KF5 

From the last few weeks, I've been working on the build system. Last week, I ported the CMake files that can use
both Qt 5 and 6 versions. I wanted to build digiKam with Qt6. All external dependency issues had been taken care of by
now and only one dependency remained that in-turn depended on Qt5, _KDE Frameworks 5_.

This week I accelerated my attempts to build KF5 components that were needed by digiKam to build with Qt6. I 
successfully built _KConfig_ and _KI18n_ with Qt6 locally. If I had more time in my hands, I would have certainly built
all required components. We decided that porting _Qt Core5Compat_ module code is more important to be completed in 
GSoC first and we can carry on building KF5 after GSoC as well. 

### Core5Compat module

The last porting step is to port code depending on [_Core5Compat_ module](https://doc.qt.io/qt-6/qtcore5-index.html).
Most simplest way is to link this module alongwith _Qt Core_. However, some manual intervention was needed in some 
instances so we decided to complete this step as we have enough time for this.

Classes we need to port:

| Core5Compat Class                                                | Degree of use in digiKam       |
|------------------------------------------------------------------|--------------------------------|
| [QRegExp](https://doc.qt.io/qt-6/qregexp.html)                   | Heavily used                   |
| [QStringRef](https://doc.qt.io/qt-6/qstringref.html)             | Used at some places            |
| [QTextCodec](https://doc.qt.io/qt-6/qtextcodec.html)             | Used at some places            |
| [QXmlSimpleReader](https://doc.qt.io/qt-6/qxmlsimplereader.html) | Used in one class              |

This week, I created these MRs for _QRegExp_, _QStringRef_:

* [Port QRegExp to QRegularExpression in dplugins](https://invent.kde.org/graphics/digikam/-/merge_requests/124)
* [Port QRegExp to QRegularExpression in libs/database](https://invent.kde.org/graphics/digikam/-/merge_requests/125)
* [Port QRegExp to QRegularExpression in core/libs](https://invent.kde.org/graphics/digikam/-/merge_requests/126)
* [Port QStringRef to QStringView](https://invent.kde.org/graphics/digikam/-/merge_requests/127)
* [Port QRegExp::exactMatch()](https://invent.kde.org/graphics/digikam/-/merge_requests/129)
* [Port QRegExp in advancedrename tool](https://invent.kde.org/graphics/digikam/-/merge_requests/130)

These are incomplete and needs review. Next week this step will continue. 

Thanks for reading!
