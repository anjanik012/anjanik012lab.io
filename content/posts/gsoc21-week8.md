---
title: "digiKam: GSoC 2021 Week 8"
date: 2021-08-08T20:34:48+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Week 8 of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/9/95/Mascot_konqi-app-utilities.png/400px-Mascot_konqi-app-utilities.png">}}

Last week's MR [Port QRegExp in advancedrename tool](https://invent.kde.org/graphics/digikam/-/merge_requests/130)
had introduced a _regression_ which was caught by the test [`advancedrename_utest`](https://invent.kde.org/graphics/digikam/-/blob/master/core/tests/advancedrename/advancedrename_utest.h). 

### About `QRegularExpression`

`QRegularExpression` implements _Perl-compatible regular expressions_. It should
be kept in mind that `QRegularExpression != QRegExp`. There are cases where a pattern can silently fail 
with `QRegularExpression` but pass with `QRegExp`. One of the key differences between these classes is that the 
result of the match of `QRegExp` is contained the object itself whereas in `QRegularExpression`, the result is
contained in another object of class `QRegularExpressionMatch`. This was an important point which I forgot to pay
attention to while porting _advancedrename_ tool.

### Regression in Advanced Rename tool

The _advancedrename_ tool API is defined in [core/utilities/advancedrename/common](https://invent.kde.org/graphics/digikam/-/tree/master/core/utilities/advancedrename/common). 
[Rule::parse()](https://invent.kde.org/graphics/digikam/-/blob/master/core/utilities/advancedrename/common/rule.h#L130)
does the regex matching. This is the implementation from master

```C++
ParseResults Rule::parse(ParseSettings &settings)
{
    ParseResults parsedResults;
    const QRegExp& reg         = regExp();
    const QString& parseString = settings.parseString;

    int pos = 0;

    while (pos > -1)
    {
        pos = reg.indexIn(parseString, pos);

        if (pos > -1)
        {
            QString result = parseOperation(settings);

            ParseResults::ResultsKey   k(pos, reg.cap(0).count());
            ParseResults::ResultsValue v(reg.cap(0), result);
            parsedResults.addEntry(k, v);
            pos           += reg.matchedLength();
        }
    }

    return parsedResults;
}
```

As one can see, the regex match is done in the line

```C++
...
while (pos > -1)
    {
        pos = reg.indexIn(parseString, pos);
...
```
is contained in the `QRegExp` object `reg`. 

The new ported line is:

```C++
...
while (pos > -1)
    {
        pos = parseString.indexOf(reg, pos);
 ...
```

The `Rule::parseOperation()` implementation also has to have the 
access to this `reg` object in-order to operate on the parsed results. For example from
[core/utilities/advancedrename/parser/options/filepropertiesoption.cpp](https://invent.kde.org/graphics/digikam/-/blob/master/core/utilities/advancedrename/parser/options/filepropertiesoption.cpp),

```C++
QString FilePropertiesOption::parseOperation(ParseSettings& settings)
{
...
    const QRegExp& reg   = regExp();
    const QString& token = reg.cap(1);
...
```

When porting this API, I realized that `reg` object will no longer have the parsed results when it is an instance of
`QRegularExpression` class. My mistake was that I assumed that I can always perform this match again when needed
in `Rule::parseOperation()`, as I had the match string `settings.parseString` and `reg` object. So I did this,

```C++
QString FilePropertiesOption::parseOperation(ParseSettings& settings)
{
...
    const QRegularExpression& reg   = regExp();
    const QString& token            = reg.match(settings.parseString).captured(1);
...
```
This approach broke some test `settings.parseString` where it was for example `[ext].[ext]`.

The string `[ext].[ext]` has two parts. The correct parsing of the string was like this:

* `[ext]` in 1st iteration
* `.[ext]` in 2nd iteration

But I got the result `[ext]` in both the iterations. I initially thought that the regex pattern had broken silently in
transition from `QRegExp` to `QRegularExpression`. But after some days, I realized that my assumption that I can 
re-match whenever I want was wrong. Match needs to be done only once in `Rule::parse()`, because it parses the whole 
string and has the role of positioning as well, whereas if re-matched the string is matched from the beginning and 
that is why I got `[ext]` in both iterations.

I patched the API to also pass the result of the [match as a second parameter](https://invent.kde.org/graphics/digikam/-/blob/gsoc21-qt6-port/core/utilities/advancedrename/common/rule.h#L145) 
to `Rule::parseOperation()`.

The patch now looks like this:

core/utilities/advancedrename/common/rule.cpp
```C++
ParseResults Rule::parse(ParseSettings &settings)
{
    ParseResults parsedResults;
    const QRegularExpression& reg         = regExp();
    const QString& parseString            = settings.parseString;
    QRegularExpressionMatch                 match;

    int pos = 0;

    while (pos > -1)
    {
        pos = parseString.indexOf(reg, pos, &match);

        if (pos > -1)
        {
            QString result = parseOperation(settings, match);

            ParseResults::ResultsKey   k(pos, match.captured(0).count());
            ParseResults::ResultsValue v(match.captured(0), result);
            parsedResults.addEntry(k, v);
            pos           += match.capturedLength();
        }
    }

    return parsedResults;
}
```

core/utilities/advancedrename/parser/options/filepropertiesoption.cpp
```C++
QString FilePropertiesOption::parseOperation(ParseSettings& settings, const QRegularExpressionMatch &match)
{
...
    const QString& token            = match.captured(1);
...
```

All tests passed after this and this got merged. It seems a silly mistake now but at the time while working, the API 
was so big that I did this mistake.😅

### Porting QTextCodec

This was trivial to port as can be seen from the [MR](https://invent.kde.org/graphics/digikam/-/merge_requests/131/).

As of now, it is not possible to port these two instances of use in digiKam,

* [QTextCodec::canEncode() has no alternative yet](https://invent.kde.org/graphics/digikam/-/blob/master/core/libs/metadataengine/engine/metaengine_exif.cpp#L304)
* [JIS7 codec is not yet supported in QStringConverter](https://invent.kde.org/graphics/digikam/-/blob/master/core/libs/metadataengine/engine/metaengine_p.cpp#L591)

So to keep this code running, we decided to keep using on _Core5Compat_ module for now.

Thanks for reading!
