---
title: "digiKam: GSoC 2021 Week 2"
date: 2021-06-20T21:00:43+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Summary of second week of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/e/e7/Mascot_konqi-app-office.png/400px-Mascot_konqi-app-office.png">}}

Hi! 

Another week has just passed and I have new things to share. This week was more maintenance work and getting ready 
before we try to build digiKam with Qt6.

In the last week, I ported a lot of code to Qt 5.15, however we need to 
maintain compatibility with at least Qt 5.12 LTS. I wrote several pre-processor checks and macros to maintain
the required compatibilty. These are some commits:

- [Version check for QButtonGroup](https://invent.kde.org/graphics/digikam/-/commit/b4d1e739efd3e50fe0a305462162431c72ebcd3f)
- [Version check for QWheelEvent](https://invent.kde.org/graphics/digikam/-/commit/34ac47afbb07a5075ed79fe187cb074becd3b7d2)
- [Version check for QLabel](https://invent.kde.org/graphics/digikam/-/commit/190039b8e2c45c6417d5d4a573a4c7d3ba246fef)
- [Version check for QFlag](https://invent.kde.org/graphics/digikam/-/commit/8bc50e72916a34e9be8e11e3519dded203a3a9af)
- [Macro for Qt::endl](https://invent.kde.org/graphics/digikam/-/commit/0a1fd2c60ec38707f0e2dfeabf6ebb45a01f9382)
- [Macro for Qt::SplitBehavior](https://invent.kde.org/graphics/digikam/-/commit/280a47d9e7ced1d1a84adba8987cddaab9768e1f)

Next, digiKam had some code that created `QSet` objects from a `QList` with `QList::toSet()` and some calls
to `QSet::fromList()` which are deprecated. Qt now enforces the use of `QSet` constructor with the container iterators.
[This merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/95) implements this. Thanks to 
[Alexander Lohnau](https://invent.kde.org/alex) for pointing out that this patch had the issue of container detachment
because of non const containers. 
Prior to this, I had worked only with STL containers and had assumed the iterators to be valid in Qt containers as well. There is always something new to learn everyday in C++. 

Now, I'm modernizing the code and using STL algorithms wherever I can to replace `for loops`. 
[cppcheck report](https://www.digikam.org/reports/cppcheck/master/) of the master branch has a lot of issues. I have to resolve 

- qSortCalled
- qrandCalled
- qsrandCalled
- useStlAlgorithm

There is a work-in-progress [merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/97) for this.

Having worked for a few weeks on this project, I can already see the evolution of Qt5 to Qt6. All the deprecations make
sense we are moving towards a better Qt. The tasks are intense but I've learned a lot. 

Thanks for reading! See y'all next week.

