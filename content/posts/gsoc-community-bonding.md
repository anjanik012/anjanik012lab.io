---
title: "digiKam: GSoC 2021 Community Bonding"
date: 2021-06-06T17:08:40+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: ""
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="/img/gsoc_kde.png">}}

Hello dear reader, I hope you are safe and sound. I am Anjani Kumar, an Information Technology student from India.
This year in [Google Summer of Code](https://summerofcode.withgoogle.com), my proposal to [Port digiKam to Qt6 on Linux](https://summerofcode.withgoogle.com/projects/#5839448035557376) 
has been selected by [KDE Community](https://kde.org). 

Today is the last day of community bonding period and from tomorrow the real work starts. 
Thanks to mentors [Gilles Caulier](https://invent.kde.org/cgilles), [Maik Qualmann](https://invent.kde.org/mqualmann) and
[Thanh Trung Dinh](https://invent.kde.org/thanhtrungdinh) for helping me prepare for the work this summer.

## First steps

I have spent these 20 days (18th May to 6th June) mostly reading the huge codebase of digiKam. This is the [branch](https://invent.kde.org/graphics/digikam/-/tree/gsoc21-qt6-port) that will have my work for GSoC'21. 

After reading enough documentation, porting guides for Qt6 and digiKam codebase we prepared a strategy to accomplish the project. 
Roughly speaking, these are the steps of the project to port to Qt6. 
- Fix all deprecated declarations of Qt5 in the existing codebase. 
- Transition to Qt6 and port code that depend on Qt5Compat module of Qt6 which is just meant to ease the porting process. So sooner we move away from it, the better.
- Test for regressions.
- Fix AppImage issues and write new scripts.

We prepared a strategy to do the first task. I will fix deprecated code class-by-class, starting from Qt classes that have fewer deprecated usages and then the latter. 
This would allow us to isolate any side-effects that my patches might introduce. 

I have started slowly fixing issues in this community bonding period. That has given me the chance to experience potential problems that I might face in the coding period and learn how to avoid them.
Also, feedback from mentors were crucial to identify some mistakes I did which were pretty bad and dangerous from my side😅.
For instance, in one patch, I applied the changes that compiler advised me to do, without thinking about the potential side-effects it was going to introduce. That was a lesson! I look forward for more.

## Next Steps

Tomorrow, I will start the work as I promised in the proposal, prepare weekly reports, and document these upcoming 10 weeks. 
Looking forward to great summer with KDE. 

Thanks for reading! Cheers!