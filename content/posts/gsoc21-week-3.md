---
title: "digiKam: GSoC 2021 Week 3"
date: 2021-06-27T18:30:25+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Summary of third week of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/9/95/Mascot_konqi-app-utilities.png/400px-Mascot_konqi-app-utilities.png">}}

Hi!

This week we fixed a lot more deprecated code. We are now very close to try building digiKam with Qt6 for the first
time.

Last week's [QSet warnings merge request was merged](https://invent.kde.org/graphics/digikam/-/merge_requests/95).

### Improving Random Number Generation in digiKam

Many image filters in digiKam rely on random number generation. Some require high-quality non-deterministic
random numbers while the others can work with pseudo random number generators seeded with system time.  

There were two ways in digiKam :- 

- Using `qsrand()` and `qrand()` from Qt often seeded with system time for common use. Now deprecated.
- Using the [RandomNumberGenerator](https://invent.kde.org/graphics/digikam/-/blob/master/core/libs/dimg/filters/randomnumbergenerator.h) class that uses `boost::random` generators and distributions. This class was written before
2011 and at that time there was no `std::random`. That is why the code had to rely on boost.

#### qsrand()/qrand()

The first option with `qsrand()/qrand()` pair was especially ugly, hard to read. For example, in this 
[file](https://invent.kde.org/graphics/digikam/-/blob/master/core/dplugins/generic/view/presentation/widgets/presentationwidget.cpp#L1223) the code generates a random number in range `[0, 15]` and checks if it is less than `6`.

```c++
if ((qrand() & 15) < 6)
{
    continue;
}
```
At the first glance, it is hardly readable and takes a few seconds to understand what it does. 

Since Qt 5.10, we now have the option to use [QRandomGenerator](https://doc.qt.io/qt-6/qrandomgenerator.html).
It is a very simple API and much more readable. It also has the ability to securely seed itself. To use it for common
purposes, it provides a thread-safe global object `QRandomGenerator::global()` and it was perfect to replace deprecated
`qrand()` and `qsrand()`. The code snippet above can be simplied as:

```c++
int num = QRandomGenerator::global()->bounded(0, 16);
if (num < 6)
{
    continue;
}
```
Readble, with no bit manipulation and clearly defined range.


These merge requests do exactly this:

- [Remove qsrand](https://invent.kde.org/graphics/digikam/-/merge_requests/104)
- [Remove qrand](https://invent.kde.org/graphics/digikam/-/merge_requests/105)

#### Porting RandomNumberGenerator class

We don't need to rely on boost to generate random numbers anymore. The task involved replacing `boost::mt19937` 
generator with `QRandomGenerator` and replace distributions like `boost::bernoulli_distribution` with `std::bernoulli_distribution` since `QRandomGenerator` is compatible with `std` generators. The [merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/106).
We have dropped one more boost dependency.

### Removing Qt X11 Extras module

The ICC color profile settings for X Window System in digiKam relies on this module which was removed in Qt 6.0.
It had to be replaced with something else. The best solution we could find was to import code 
of `QX11Info` class from Qt source tree. This class will land in Qt 6.2.0 again in September this year 
as a private API but for now we needed to find solution so we can build digiKam with Qt6 without removing this feature. 
This is currently [under review](https://invent.kde.org/graphics/digikam/-/merge_requests/107).

### Replacing QMutex with QRecursiveMutex

This is one of the few remaining class which is partially deprecated in Qt6 and is used in digiKam. So we just replace
`QMutex` with `QRecursiveMutex` wherever needed with some Qt version checking since `QRecursiveMutex` was introduced in 
Qt 5.14. The [merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/108) under review.

### Summary

A lot happened this week. Almost all major deprecation warnings have been resolved. The master branch build log is
__6693__ lines long because of all the warnings.
The [fix-QMutex](https://invent.kde.org/graphics/digikam/-/tree/fix-QMutex) branch has just __886__ lines now. That
is almost __87%__ less than master. Very few deprecated warnings remain and most of them are not related to Qt.

So, in coming weeks I will try for the first time to build digiKam with Qt6 :)

Thanks for reading! Cheers!

