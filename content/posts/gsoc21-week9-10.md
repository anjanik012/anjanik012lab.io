---
title: "digiKam: GSoC 2021 Week 9-10"
date: 2021-08-15T11:11:30+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Last weeks of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/9/95/Mascot_konqi-app-utilities.png/400px-Mascot_konqi-app-utilities.png">}}

We are here at the end of the coding period. I spent the last two weeks doing the last few remaining tasks and some
more regression fixing. 

### Regression again with QRegularExpression

This time the regression was in [core/utilities/import/views/cameranamehelper.cpp](https://invent.kde.org/graphics/digikam/-/blob/master/core/utilities/import/views/cameranamehelper.cpp). I used the same strategy to patch this class
as I had used with others. The `cameranamehelper_utest` mysteriously failed.

Upon debugging I narrowed it down to the line 

```C++
static QRegExp REGEXP_MODES(
    QLatin1String("^(ptp|normal|mtp)(\\s+mode)?$"), 
    Qt::CaseInsensitive);
```

which was ported to

```C++
static QRegularExpression REGEXP_MODES(
    QRegularExpression::anchoredPattern(QLatin1String("^(ptp|normal|mtp)(\\s+mode)?$")),
    QRegularExpression::CaseInsensitiveOption);

```

`QRegularExpression::anchoredPattern()` is used here to do _exact matching_. This should have worked. On debugging and
trying out new approaches, I tried this

```C++
static QRegularExpression REGEXP_MODES(
    QRegularExpression::anchoredPattern(QLatin1String("(?i)^(ptp|normal|mtp)(\\s+mode)?$")));

```
and it worked.

I just added `(?i)` to the pattern instead of using the option `QRegularExpression::CaseInsensitiveOption`. It was 
strange. According to the docs as well, the option is equivalent to the string I added manually to the pattern and
it should have worked. We wanted to use the option so that the code is more readable.

After making a few more trials, I found a way to make the option work

```C++
QRegularExpression REGEXP_MODES(
    QRegularExpression::anchoredPattern(QLatin1String("(ptp|normal|mtp)(\\s+mode)?")),
    QRegularExpression::InvertedGreedinessOption | QRegularExpression::CaseInsensitiveOption
);
```

`QRegularExpression::CaseInsenstiveOption` worked only when used with _OR_ with this option. This was strange is
a probably a bug in Qt. This was [merged](https://invent.kde.org/graphics/digikam/-/merge_requests/132).

### Porting the AppImage scripts

There are 4 scripts to build appimages.

* Build host (Build Qt)
* Build extra libs (Build KF5)
* Build digiKam
* Build appimage

and some more helper scripts. Because we left building KF5 for now we can only port the first script to set up the 
build environment in _Mageia 7.1_ and install Qt6. We selected Qt `6.2.0-beta2` as Qt 6.1 is not ready yet
with all the dependencies requied for digiKam. This is the MR. I am currently working on the script. Here is the
[merge request](https://invent.kde.org/graphics/digikam/-/merge_requests/138).

Thanks for reading!
