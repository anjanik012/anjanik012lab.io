---
title: "digiKam: GSoC 2021 Week 1"
date: 2021-06-14T11:29:00+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Summary of first week of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/7/79/Mascot_konqi-app-dev.png/400px-Mascot_konqi-app-dev.png">}}

So, the first week of coding period has ended. It was exciting and full of challenges. I am happy that I am on the right 
track and making progress as I've promised. This is a small summary of the work done this week.

## Getting rid of compiler warnings

digiKam has a really huge codebase. It is actively developed but a lot of code is old which contains deprecated Qt code 
which must be fixed before introducing Qt6. These are the classes which had issues in digiKam which are now fixed :- 

- QMap
- QProcess
- QHash
- QMatrix
- QLabel
- QString
- QFlag
- QTimeLine
- QTabletEvent
- QWheelEvent
- QButtonGroup
- QPrinter
- Some namespace changes like `endl` to `Qt::endl`

These patches can be found at my work [branch](https://invent.kde.org/graphics/digikam/-/commits/gsoc21-qt6-port)

And these remain which I shall fix this week:

- QSet
- `qrand()` and `qsrand()`

Regression testing is important for my project. I patched an important class in the core of digiKam that was used by all
filters in the image editor ([The MR](https://invent.kde.org/graphics/digikam/-/merge_requests/90)).
This had a risk of introducing regression to filters. [I wrote a new unit test](https://invent.kde.org/graphics/digikam/-/merge_requests/91) to verify it did not. 

In a nutshell, there are currently __6693__ lines of build warnings on [master](https://invent.kde.org/graphics/digikam/-/commits/master). [See this here](https://gist.github.com/anjanik012/cd7edc7a537e54338b4b222c42a595ad)

With the patches on my [branch](https://invent.kde.org/graphics/digikam/-/commits/gsoc21-qt6-port) these warnings have reduced
to __1830__ lines! That's a reduction of about __73%__. [See this here](https://gist.github.com/anjanik012/356336a3694f99ff9f57087fcdcd78a9).

While this is not the best way to track progress but it still gives rough estimate. We now have much cleaner
build outputs. This week, it will get smaller.

Thanks for reading!
