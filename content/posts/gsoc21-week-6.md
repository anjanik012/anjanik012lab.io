---
title: "digiKam: GSoC 2021 Week 6"
date: 2021-07-25T20:25:13+05:30
draft: false
tags: ["open-source", "GSoC", "kde", "digiKam"]
author: "Me"
# author: ["Me", "You"] # multiple authors
# showToc: true
TocOpen: false
hidemeta: false
comments: false
description: "Week 6 of Google Summer of Code 2021"
canonicalURL: "https://canonical.url/to/page"
---

{{< figure align=center src="https://community.kde.org/images.community/thumb/c/c8/Mascot_konqi-app-system.png/400px-Mascot_konqi-app-system.png">}}

Hi, the first evaluation is complete. My mentors said that they have "enjoyed" my work lately so that was good to hear.

Previously, we had fully ported digiKam to Qt `5.15.2`. So now it was time to introduce Qt6. First steps were to 
identify the required changes in the build system, checking and verifying dependencies.

### Porting the build system

digiKam has a huge codebase and thus, a huge build system which is very finely tuned. So, it took some to time to fully
study the build system and identify the required changes to it. 

Our goal was that the build system should find a suitable Qt version, i.e., 5 or 6 and then accordingly generate
build files. The current pipelines and tests with Qt5 should work as before and if the system finds Qt6 installation,
then configure accordingly. All of the includes and linking steps in the `CMakeLists.txt`s had hardcoded Qt 5 version.

I had attended this year's "Akademy" and learned a great deal about porting the build system from the talk [Porting user applications to Qt 6](https://conf.tube/videos/watch/playlist/38bfc51e-253d-4976-ba1c-1a16bcc6d9b0?playlistPosition=17&resume=true) by _Kai Köhne_. 

I used cmake variables instead of hardcoding Qt versions, the MRs
- [Port CMake files](https://invent.kde.org/graphics/digikam/-/merge_requests/119).
- [Link Core5Compat module](https://invent.kde.org/graphics/digikam/-/merge_requests/120)

I made some commits without an MR as similar patches were already approved. Most of these were patching include 
directories in cmake and had hardcoded Qt5 version. An example, 
[Port cmake include dirs in bqm/metadata](https://invent.kde.org/graphics/digikam/-/commit/2b41dca829ec8976a7889b8c0beb7680772566b4)

Thanks for reading!
